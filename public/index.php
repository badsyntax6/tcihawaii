<?php 

// Start a new session.
if (!isset($_SESSION)) { session_start(); }

// Define Gusto version.
define('BUILD', '19.9.18-07.46.13');

// Include Gusto config.
require_once str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']) . '/../private/config/settings.php';

// Run install script if present.
if (is_file(PRIVATE_DIR . '/install/startup.php')) {
    require_once PRIVATE_DIR . '/install/startup.php';
} else {
    // Include start files.
    require_once PRIVATE_DIR . '/autoload.php';
    Application::getInstance();
}

/**
 * This function will check if the first index in the url_arrayd URL array is the string admin.
 * 
 * @return boolean - Simple true or false.
 */
function isAdmin()
{
    if (isset($_GET['url'])) {
        $url_array = explode('/', filter_var(trim($_GET['url'], '/'), FILTER_SANITIZE_URL));
    }

    if (isset($url_array[0])) {
        if ($url_array[0] == 'admin') {
            return true;
        } else {
            return false;
        }
    }
}