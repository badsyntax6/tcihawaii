/**
 * Common Javascript 
 *
 * Common js used site wide. Most of this will be used on every page because
 * a lot of it is for the header and navigation stuff. Dropdown menus, to 
 * top buttons, ect.
 */
var Common = new Object();

$(window).on('load', function() {
    Object.keys(Common).forEach(function(key) {
        var func = Common[key];
        if (typeof func === 'function' && key) {
            try {
                func();
            } catch (error) {
                $('body').prepend(error.stack + '\n');
                window.onerror = function(error, file, line, col) {
                    $('body').prepend(error + ' File: ' + file + ' Line: ' + line + ' Col: ' + col + '\n');
                    return false;
                };
            }
        }
    });
}); 

// Drop menus
Common.dropMenusDown = function() {
    $(document).on('click', '.dropdown-button', function() {
        $(this).siblings().find('.dropdown-menu').removeClass('active');
        $('.dropdown-menu', this).toggleClass('active');
        $('i', this).toggleClass('fa-caret-up');
        $(this).siblings().find('i').removeClass('fa-caret-up');
    });
}

Common.mobileMenuClick = function() {
    $('body').on('click', '.btn-mobile-menu', function() {
        $('.nav').toggleClass('open');
    });  
}

// Go to top button
Common.goToTop = function() {
    $('.main').append('<div class="to-top"><i class="fas fa-angle-up"></i> Scroll Up</div>');
    $(window).scroll(function() {
        if ($(this).scrollTop() > 520) {
            $('.to-top').addClass('visible');
        } else {
            $('.to-top').removeClass('visible');
        }

        if (!$('.nav').hasClass('wide')) {
            $('.to-top').css({'width' : 65 + 'px', 'overflow' : 'hidden'});
            $('.to-top i').css({'width' : 65 + 'px'});
        } else {
            $('.to-top').removeAttr('style');
            $('.to-top i').removeAttr('style');
        }
    });
    $('.to-top').on('click', function() {
        $('html, body').animate( {
            scrollTop: 0
        }, 300);
    });
}

// Hightlight nav link
Common.highlightNavLink = function() {
    if ($('ul').hasClass('main-nav')) {
        var pieces = window.location.pathname.split('/');
        var url = pieces[2];
        var nav_class = url ? '.nav-link.' + url : '.nav-link.dashboard';

        $(nav_class).addClass('current');
        $('.nav-link.current').parent().parent().addClass('active');   
    }
}

Common.showLoader = function(param) {
    $(param).prepend('<div class="loading"><div class="loading-spinner"><i class="fas fa-circle-notch fa-fw fa-spin"></i></div></div>');
}

Common.removeLoader = function() {
    $('.loading').remove();
}

Common.goBack = function() {
    var breadcrumb = window.location.pathname.split('/');
    breadcrumb.pop();
    
    if (breadcrumb && breadcrumb.length == 1) {
        breadcrumb[0] = '/';
    }

    var back = breadcrumb.join('/');

    $('.btn-goback').prop('href', back);
}

Common.changeThemeSetting = function() {
    $('.account-nav').on('click', '.theme-light', function() {
        $.post('/settings/changeThemeSetting', {'theme' : 0}, function(response) {
            if ($.trim(response)) {
                if (response == 0) {
                    $('body').removeClass('dark-theme');
                    $('body').addClass('light-theme');
                }
            }
        });
    });

    $('.account-nav').on('click', '.theme-dark', function() {
        $.post('/settings/changeThemeSetting', {'theme' : 1}, function(response) {
            if ($.trim(response)) {
                if (response == 1) {
                    $('body').removeClass('light-theme');
                    $('body').addClass('dark-theme');
                }
            }
        });
    });
}

Common.getMenuSetting = function() {
    $.get('/settings/getMenuSetting', function(response) {
        if ($.trim(response)) {
            if (response == 0) {
                $('.btn-menu i').addClass('fa-toggle-off').removeClass('fa-toggle-on');
            }
            if (response == 1) {
                $('.btn-menu i').addClass('fa-toggle-on').removeClass('fa-toggle-off');
            }
        }
    });
}

Common.changeMenuSetting = function() {
    if ($(window).width() > 1024) {
        $('.main-nav').on('click', '.btn-menu', function() {
            $.get('/settings/changeMenuSetting', function(response) {
                if ($.trim(response)) {
                    if (response == 0) {
                        $('.btn-menu i').removeClass('fa-toggle-on').addClass('fa-toggle-off');
                        $('.nav').removeClass('wide');
                    }
                    if (response == 1) {
                        $('.btn-menu i').addClass('fa-toggle-on').removeClass('fa-toggle-off');
                        $('.nav').addClass('wide');
                    }
                }
            });
            
            return false;
        });
    }
}