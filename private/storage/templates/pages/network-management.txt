<div class="network-management-content">
    <div class="row solutions-banner">
        <div class="wrapper">
            <h1>Network Management</h1>
        </div> 
    </div>
    <div class="row space-top-60 solutions-content">
        <div class="wrapper">
            <div class="row">
                <h2 class="row space-bottom-15 heading-medium">You've built a great network. Let's keep it that way.</h2>
                <p class="row space-bottom-15">Building a great network is what we do.  Making a mess of it is what all the vendors and consultants that come after us do.  It doesn't have to be that way! Our managed network service is how
                you outsource all of your <I>M</I>oves, <I>A</I>dds and <i>C</i>hanges (MACs).  When a network is on our managed service we handle all third party interfacing as well as your internal needs.</p>
                <p>Our network management service comes standard with:</p>
                <ul class="list-style-check space-top-15 space-bottom-30 space-left-30">
                    <li>Hardware guarantees</li>
                    <li>Service Level Agreements for response time</li>
                    <li>Standard communication methods and issue tracking</li>
                    <li>Internal support for your users: Move a wire, add a wire, change VLANs, change security, and so on.</li>
                    <li>External support for your vendors and consultants: We configure ports, facilitate VPN access, DMZ settings and more.</li>
                    <li>Mesh wifi if needed is managed via locally deployed controller units and remotely configured.</li>
                    <li>Bad wire?  Weird issues?  We repair or restring any defective line or cable.</li>
                </ul>
                <p>We provide this service for a simple flat rate based on the size of the network we are managing.  There are no costs for hours used or 'unexpected' fees.</p>
            </div>
            <div class="row space-top-60">
                <h3 class="space-bottom-15 heading-medium">Routers and Switches</h3>
                <p><a href="/help-guide#routers">Routers</a> and <a href="help-guide#switches">switches</a> make up the core of your network.  We use equipment that meets several criteria: It must support VLANs and sub-interfaces, it must be remotely manageable, and it must
                offer security features to prevent unauthorized access.  For this reason we deploy business class routers and fully managed switches as part of our service.</p>
            </div>
            <div class="row space-top-60">
                <h5 class="space-bottom-15 heading-medium">WiFi</h5>
                <p>Our Wifi equipment is selected for range, ease of deployment, ease of management, and for expansion. Our focus is that the client does not waste time tinkering with complicated solutions that offer expansion.  We want the client to be able to 'drop-in and expand' their wifi network with minimal interruption to the work day.</p>
            </div>
            <div class="row space-top-60">
                <h4 class="space-bottom-15 heading-medium">Cable Management</h4>
                <p>Cables and their arrangement may not seem like the most glamorous part of your network but when done wrong it becomes a depressing eyesore in your workplace.  When done right you'll be
                smiling everytime you pass by your switches.  We know that feeling and we want you to enjoy it as well.  We do this by removing all cable clutter and then installing based on our personalized
                cabling system:</p>
                <ul class="row list-style-check space-top-15 space-left-30">
                    <li>Unified patch cable colors and length</li>
                    <li>Hidden cable slack</li>
                    <li>No space between switches, routers, cable managers, and patch panels</li>
                    <li>We deploy official 'Neatpatch' brand managers</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row space-top-60 pad-ver-30 bkg-red text-white text-center get-service-now">
        <h6 class="heading-medium text-white">Two ways to get this service immediately</h6>
        <a href="/contact" class="btn btn-alt space-top-30">Contact us now <i class="fas fa-chevron-right fa-fw"></i></a>
    </div>
</div>