<?php 

/**
 * Output Core Class
 * 
 * Output different types of data to the view.
 */
class Output
{
    /**
     * Output JSON
     *
     * Take an array and convert to json then tepending on
     * the action exit, return or echo the json.
     * 
     * @param array $array
     * @param boolean $action
     * @return void
     */
    public function json($array, $action = false)
    {
        switch ($action) {
            case 'exit':
                exit(json_encode($array));
                break;
            case 'return':
                return json_encode($array);
                break;
            default:
                echo json_encode($array);
                break;
        }
    }

    /**
     * Output Text
     * 
     * Return plain text.
     *
     * @param string $text
     * @return void
     */
    public function text($content)
    {
        $content = preg_replace('@<script[^>]*?>.*?</script>@si', '', $content);
        $content = preg_replace('@<style[^>]*?>.*?</style>@si', '', $content);
        $content = strip_tags($content);
        $content = trim($content);
        return $content;
    }

    /**
     * Output HTML
     * 
     * Echo HTML
     *
     * @param string $view
     * @param array $data
     * @return void
     */
    public function html($view, $data = [])
    {
        $theme_dir = $this->load->model('settings')->getSetting('theme');
        $file = PUBLIC_DIR . '/front/themes/' . $theme_dir . '/htm/' . $view . '.htm';

        if (isAdmin()) {
            $file = PUBLIC_DIR . '/admin/htm/' . $view . '.htm';
        }
        
        if (is_array($data)) extract($data);

        if (is_file($file)) {
            ob_start();
            require($file);
            echo ob_get_clean();
        }
    }
}