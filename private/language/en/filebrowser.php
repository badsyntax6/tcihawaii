<?php 

/**
 * Alerts
 */
$_['folder_created'] = '<div class="row alert success"><strong>Success!</strong> Your folder has been created. <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';
$_['folder_exists'] = '<div class="row alert error"><strong>Error!</strong> A folder with that name already exists. <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';
$_['file_deleted'] = '<div class="row alert success"><strong>Success!</strong> Your file has been deleted. <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';
$_['folder_deleted'] = '<div class="row alert success"><strong>Success!</strong> Your folder has been deleted. <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';
$_['system_file'] = '<div class="row alert warning"><strong>Warning!</strong> This file cannot be deleted. <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';
// File Uploads
$_['file_invalid'] = 'File denied. Excepted files types for avatars are: jpg | jpeg | png | gif.';
$_['file_big'] = 'File is too big ({{filesize}}MB). Max filesize: {{maxFilesize}}MB.';
$_['upload_failure'] = 'Your file could not be uploaded for unknown reasons.';
$_['upload_success'] = 'Your file has been uploaded.';