<?php

/**
 * Alerts
 */
$_['file_invalid'] = 'File denied. Excepted files types for avatars are: jpg | jpeg | png | gif.';
$_['file_big'] = 'File is too big ({{filesize}}). Max filesize: {{maxFilesize}}.';
$_['fail'] = 'Your file has not been uploaded.';
$_['success'] = 'Your file has been uploaded.';