<?php 

/**
 * Header Controller Class
 *
 * The HeaderController handles logic specific to the header and displays the 
 * header view. The HeaderController should be loaded in each controller class 
 * where a header is desired.
 */
class AdminHeaderController extends Controller
{
    /**
     * Index method
     *
     * The index methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Assuming that the HeaderController should be loaded in every controller 
     * this index method should run in every controller too.
     */
    public function index($title = false)
    {    
        $url = isset($_GET['url']) ? $this->helper->splitUrl($_GET['url']) : null;

        if ($title) {
            $view['title'] = $title;
        } else {
            $view['title'] = isset($url[1]) ? $this->language->get($url[1] . '/title') : 'Gusto';
        }

        $view['logged'] = $this->logged_user;
        $view['theme_text'] = $this->language->get('header/theme_text');
        $view['view_text'] = $this->language->get('header/view_text');
        $view['account_text'] = $this->language->get('header/account_text');
        $view['breadcrumb'] = $this->load->controller('admin/breadcrumb')->index();
        $view['search'] = $this->load->controller('admin/search')->index();

        return $this->load->view('common/header', $view);
    }
}