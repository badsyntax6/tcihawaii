<?php 
 
/**
 * WhyTciController Controller Class
 */
class WhyTciController extends Controller
{
    /**
     * Init method
     *
     * The init method is the default for controller classes. Whenever a controller
     * class is instantiated the init method will be called.
     */
    public function index()
    {
        $page = $this->load->model('pages')->getPage('name', 'why-tci');
 
        $data['title'] = $page['title'];
        $data['description'] = $page['description'];
 
        $view['header'] = $this->load->controller('header')->index($data);
        $view['footer'] = $this->load->controller('footer')->index();
        $view['content'] = $this->load->model('pages')->getPageContent('why-tci');
 
        $this->load->model('pages')->updatePageStatistics('why-tci');
 
        exit($this->load->view('common/content', $view));
    }
}