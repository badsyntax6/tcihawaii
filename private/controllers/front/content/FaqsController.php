<?php 
 
/**
 * FaqsController Controller Class
 */
class FaqsController extends Controller
{
    /**
     * Init method
     *
     * The init method is the default for controller classes. Whenever a controller
     * class is instantiated the init method will be called.
     */
    public function index()
    {
        $page = $this->load->model('pages')->getPage('name', 'faqs');
 
        $data['title'] = $page['title'];
        $data['description'] = $page['description'];
 
        $view['header'] = $this->load->controller('header')->index($data);
        $view['footer'] = $this->load->controller('footer')->index();
        $view['content'] = $this->load->model('pages')->getPageContent('faqs');
 
        $this->load->model('pages')->updatePageStatistics('faqs');
 
        exit($this->load->view('common/content', $view));
    }
}