<?php 
 
/**
 * WirelessAndP2pController Controller Class
 */
class WirelessAndP2pController extends Controller
{
    /**
     * Init method
     *
     * The init method is the default for controller classes. Whenever a controller
     * class is instantiated the init method will be called.
     */
    public function index()
    {
        $page = $this->load->model('pages')->getPage('name', 'wireless-and-p2p');
 
        $data['title'] = $page['title'];
        $data['description'] = $page['description'];
 
        $view['header'] = $this->load->controller('header')->index($data);
        $view['footer'] = $this->load->controller('footer')->index();
        $view['content'] = $this->load->model('pages')->getPageContent('wireless-and-p2p');
 
        $this->load->model('pages')->updatePageStatistics('wireless-and-p2p');
 
        exit($this->load->view('common/content', $view));
    }
}